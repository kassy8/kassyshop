import React, {Component} from 'react';
import sections from '../../directory.data.js';
import MenuItem from '../../components/menu-item/menu-item.component';
import './directory.styles.scss';

class Directory extends Component{
    constructor(){
        super();
        this.state= {
            sections: sections
        }
    }

    render(){
        return(
        <div className="directory-menu">
        {
            this.state.sections.map(({id,...otherSectionProps}) => {
                return <MenuItem key={id} {...otherSectionProps} />;
            })
        }
        </div>
        );
    }
}

export default Directory;