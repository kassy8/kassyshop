
import React, {Component} from 'react';
import {Switch,Route} from 'react-router-dom';
import './App.css';

import Homepage from './pages/homepage/homepage.component';
import Shoppage from './pages/shop/shop.component';
import Header from './components/header/header.component';
import SignUpSignIn from './pages/signup-signin/signup-signin.component';
import {auth} from './firebase/firebase.utils';

class App extends Component {

  constructor(){
    super()
    this.state= {
      currentUser: null
    }
  }

  unsubscribeFromAuth = null;

  componentDidMount(){
    auth.onAuthStateChanged(user=>{
      this.setState({currentUser: user});
    })
  }

  componentWillUnmount(){
    this.unsubscribeFromAuth();
  }

  render(){
    return (
      <div>
        <Header currentUser={this.state.currentUser} />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/shop" component={Shoppage} />
          <Route exact path="/signin" component={SignUpSignIn} />
        </Switch>
      </div>
    );    
  }

}

export default App;
