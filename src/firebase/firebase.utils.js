import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyANo_LkIc4YMQfSzlc7-iqwQbPMUfpt1Ck",
    authDomain: "kassyshop-c72fb.firebaseapp.com",
    databaseURL: "https://kassyshop-c72fb.firebaseio.com",
    projectId: "kassyshop-c72fb",
    storageBucket: "",
    messagingSenderId: "623474516299",
    appId: "1:623474516299:web:c626722f891fb84b30c8ce"
};

firebase.initializeApp(config);


export const auth= firebase.auth();
export const firestore= firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;